# Advice from The C++ Programming Language, Fourth Edition

Each chapter contains an “Advice” section with a set of concrete recommendations related to its contents. Such advice consists of rough rules of thumb, not immutable laws. A piece of advice should be applied only where reasonable. There is no substitute for intelligence, experience, common sense, and good taste.

## 1. Notes to the Reader

1. Represent ideas (concepts) directly in code, for example, as a function, a class, or an enumeration; [§1.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch01.html#ch1lev1sec2).

1. Aim for your code to be both elegant and efficient; [§1.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch01.html#ch1lev1sec2).

1. Don’t overabstract; [§1.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch01.html#ch1lev1sec2).

1. Focus design on the provision of elegant and efficient abstractions, possibly presented as libraries; [§1.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch01.html#ch1lev1sec2).

1. Represent relationships among ideas directly in code, for example, through parameterization or a class hierarchy; [§1.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch01.html#ch1lev2sec2sec1).

1. Represent independent ideas separately in code, for example, avoid mutual dependencies among classes; [§1.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch01.html#ch1lev2sec2sec1).

1. C++ is not just object-oriented; [§1.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch01.html#ch1lev2sec2sec1).

1. C++ is not just for generic programming; [§1.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch01.html#ch1lev2sec2sec1).

1. Prefer solutions that can be statically checked; [§1.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch01.html#ch1lev2sec2sec1).

1. Make resources explicit (represent them as class objects); [§1.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch01.html#ch1lev2sec2sec1), [§1.4.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch01.html#ch1lev3sec4sec2-1).

1. Express simple ideas simply; [§1.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch01.html#ch1lev2sec2sec1).

1. Use libraries, especially the standard library, rather than trying to build everything from scratch; [§1.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch01.html#ch1lev2sec2sec1).

1. Use a type-rich style of programming; [§1.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch01.html#ch1lev2sec2sec2).

1. Low-level code is not necessarily efficient; don’t avoid classes, templates, and standard-library components out of fear of performance problems; [§1.2.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch01.html#ch1lev2sec2sec4), [§1.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch01.html#ch1lev2sec3sec3).

1. If data has an invariant, encapsulate it; [§1.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch01.html#ch1lev2sec3sec2).

1. C++ is not just C with a few extensions; [§1.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch01.html#ch1lev2sec3sec3).

## 2. A Tour of C++: The Basics

1. Don’t panic! All will become clear in time; [§2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch02.html#ch2lev1sec1).

1. You don’t have to know every detail of C++ to write good programs; [§1.3.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch01.html#ch1lev2sec3sec1).

1. Focus on programming techniques, not on language features; [§2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch02.html#ch2lev1sec1).

## 3. A Tour of C++: Abstraction Mechanisms

1. Express ideas directly in code; [§3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch03.html#ch3lev1sec2).

1. Define classes to represent application concepts directly in code; [§3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch03.html#ch3lev1sec2).

1. Use concrete classes to represent simple concepts and performance-critical components; [§3.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch03.html#ch3lev2sec2sec1).

1. Avoid “naked” new and delete operations; [§3.2.1.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch03.html#ch3lev3sec2sec1-2).

1. Use resource handles and RAII to manage resources; [§3.2.1.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch03.html#ch3lev3sec2sec1-2).

1. Use abstract classes as interfaces when complete separation of interface and implementation is needed; [§3.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch03.html#ch3lev2sec2sec2).

1. Use class hierarchies to represent concepts with inherent hierarchical structure; [§3.2.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch03.html#ch3lev2sec2sec4).

1. When designing a class hierarchy, distinguish between implementation inheritance and interface inheritance; [§3.2.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch03.html#ch3lev2sec2sec4).

1. Control construction, copy, move, and destruction of objects; [§3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch03.html#ch3lev1sec3).

1. Return containers by value (relying on move for efficiency); [§3.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch03.html#ch3lev2sec3sec2).

1. Provide strong resource safety; that is, never leak anything that you think of as a resource; [§3.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch03.html#ch3lev2sec3sec3).

1. Use containers, defined as resource-handle templates, to hold collections of values of the same type; [§3.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch03.html#ch3lev2sec4sec1).

1. Use function templates to represent general algorithms; [§3.4.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch03.html#ch3lev2sec4sec2).

1. Use function objects, including lambdas, to represent policies and actions; [§3.4.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch03.html#ch3lev2sec4sec3).

1. Use type and template aliases to provide a uniform notation for types that may vary among similar types or among implementations; [§3.4.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch03.html#ch3lev2sec4sec5).

## 4. A Tour of C++: Containers and Algorithms

1. Don’t reinvent the wheel; use libraries; [§4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch04.html#ch4lev1sec1).

1. When you have a choice, prefer the standard library over other libraries; [§4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch04.html#ch4lev1sec1).

1. Do not think that the standard library is ideal for everything; [§4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch04.html#ch4lev1sec1).

1. Remember to #include the headers for the facilities you use; [§4.1.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch04.html#ch4lev2sec1sec2).

1. Remember that standard-library facilities are defined in namespace std; [§4.1.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch04.html#ch4lev2sec1sec2).

1. Prefer strings over C-style strings (a char*; [§2.2.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch02.html#ch2lev2sec2sec5)); [§4.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch04.html#ch4lev1sec2), [§4.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch04.html#ch4lev2sec3sec2).

1. iostreams are type-sensitive, type-safe, and extensible; [§4.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch04.html#ch4lev1sec3).

1. Prefer vector\<T>, map\<K,T>, and unordered_map\<K,T> over T[]; [§4.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch04.html#ch4lev1sec4).

1. Know your standard containers and their tradeoffs; [§4.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch04.html#ch4lev1sec4).

1. Use vector as your default container; [§4.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch04.html#ch4lev2sec4sec1).

1. Prefer compact data structures; [§4.4.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch04.html#ch4lev3sec4sec1-1).

1. If in doubt, use a range-checked vector (such as Vec); [§4.4.1.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch04.html#ch4lev3sec4sec1-2).

1. Use push_back() or back_inserter() to add elements to a container; [§4.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch04.html#ch4lev2sec4sec1), [§4.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch04.html#ch4lev1sec5).

1. Use push_back() on a vector rather than realloc() on an array; [§4.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch04.html#ch4lev1sec5).

1. Catch common exceptions in main(); [§4.4.1.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch04.html#ch4lev3sec4sec1-2).

1. Know your standard algorithms and prefer them over handwritten loops; [§4.5.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch04.html#ch4lev2sec5sec5).

1. If iterator use gets tedious, define container algorithms; [§4.5.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch04.html#ch4lev2sec5sec6).

1. You can use the range-for loop for a complete container.

## 5. A Tour of C++: Concurrency and Utilities

1. Use resource handles to manage resources (RAII); [§5.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch05.html#ch5lev1sec2).

1. Use unique_ptr to refer to objects of polymorphic type; [§5.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch05.html#ch5lev2sec2sec1).

1. Use shared_ptr to refer to shared objects; [§5.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch05.html#ch5lev2sec2sec1).

1. Use type-safe mechanisms for concurrency; [§5.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch05.html#ch5lev1sec3).

1. Minimize the use of shared data; [§5.3.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch05.html#ch5lev2sec3sec4).

1. Don’t choose shared data for communication because of “efficiency” without thought and preferably not without measurement; [§5.3.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch05.html#ch5lev2sec3sec4).

1. Think in terms of concurrent tasks, rather than threads; [§5.3.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch05.html#ch5lev2sec3sec5).

1. A library doesn’t have to be large or complicated to be useful; [§5.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch05.html#ch5lev1sec4).

1. Time your programs before making claims about efficiency; [§5.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch05.html#ch5lev2sec4sec1).

1. You can write code to explicitly depend on properties of types; [§5.4.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch05.html#ch5lev2sec4sec2).

1. Use regular expressions for simple pattern matching; [§5.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch05.html#ch5lev1sec5).

1. Don’t try to do serious numeric computation using only the language; use libraries; [§5.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch05.html#ch5lev1sec6).

1. Properties of numeric types are accessible through numeric_limits; [§5.6.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch05.html#ch5lev2sec6sec5).

## 6. Types and Declarations

1. For the final word on language definition issues, see the ISO C++ standard; [§6.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch06.html#ch6lev1sec1).

1. Avoid unspecified and undefined behavior; [§6.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch06.html#ch6lev1sec1).

1. Isolate code that must depend on implementation-defined behavior; [§6.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch06.html#ch6lev1sec1).

1. Avoid unnecessary assumptions about the numeric value of characters; [§6.2.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch06.html#ch6lev3sec2sec3-2), [§10.5.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch10.html#ch10lev3sec5sec2-1).

1. Remember that an integer starting with a 0 is octal; [§6.2.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch06.html#ch6lev3sec2sec4-1).

1. Avoid “magic constants”; [§6.2.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch06.html#ch6lev3sec2sec4-1).

1. Avoid unnecessary assumptions about the size of integers; [§6.2.8](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch06.html#ch6lev2sec2sec8).

1. Avoid unnecessary assumptions about the range and precision of floating-point types; [§6.2.8](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch06.html#ch6lev2sec2sec8).

1. Prefer plain char over signed char and unsigned char; [§6.2.3.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch06.html#ch6lev3sec2sec3-1).

1. Beware of conversions between signed and unsigned types; [§6.2.3.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch06.html#ch6lev3sec2sec3-1).

1. Declare one name (only) per declaration; [§6.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch06.html#ch6lev2sec3sec2).

1. Keep common and local names short, and keep uncommon and nonlocal names longer; [§6.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch06.html#ch6lev2sec3sec3).

1. Avoid similar-looking names; [§6.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch06.html#ch6lev2sec3sec3).

1. Name an object to reflect its meaning rather than its type; [§6.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch06.html#ch6lev2sec3sec3).

1. Maintain a consistent naming style; [§6.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch06.html#ch6lev2sec3sec3).

1. Avoid ALL_CAPS names; [§6.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch06.html#ch6lev2sec3sec3).

1. Keep scopes small; [§6.3.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch06.html#ch6lev2sec3sec4).

1. Don’t use the same name in both a scope and an enclosing scope; [§6.3.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch06.html#ch6lev2sec3sec4).

1. Prefer the {}-initializer syntax for declarations with a named type; [§6.3.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch06.html#ch6lev2sec3sec5).

1. Prefer the = syntax for the initialization in declarations using auto; [§6.3.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch06.html#ch6lev2sec3sec5).

1. Avoid uninitialized variables; [§6.3.5.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch06.html#ch6lev3sec3sec5-1).

1. Use an alias to define a meaningful name for a built-in type in cases in which the built-in type used to represent a value might change; [§6.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch06.html#ch6lev1sec5).

1. Use an alias to define synonyms for types; use enumerations and classes to define new types; [§6.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch06.html#ch6lev1sec5).

## 7. Pointers, Arrays, and References

1. Keep use of pointers simple and straightforward; [§7.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch07.html#ch7lev2sec4sec1).

1. Avoid nontrivial pointer arithmetic; [§7.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch07.html#ch7lev1sec4).

1. Take care not to write beyond the bounds of an array; [§7.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch07.html#ch7lev2sec4sec1).

1. Avoid multidimensional arrays; define suitable containers instead; [§7.4.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch07.html#ch7lev2sec4sec2).

1. Use nullptr rather than 0 or NULL; [§7.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch07.html#ch7lev2sec2sec2).

1. Use containers (e.g., vector, array, and valarray) rather than built-in (C-style) arrays; [§7.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch07.html#ch7lev2sec4sec1).

1. Use string rather than zero-terminated arrays of char; [§7.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch07.html#ch7lev1sec4).

1. Use raw strings for string literals with complicated uses of backslash; [§7.3.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch07.html#ch7lev3sec3sec2-1).

1. Prefer const reference arguments to plain reference arguments; [§7.7.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch07.html#ch7lev2sec7sec3).

1. Use rvalue references (only) for forwarding and move semantics; [§7.7.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch07.html#ch7lev2sec7sec2).

1. Keep pointers that represent ownership inside handle classes; [§7.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch07.html#ch7lev1sec6).

1. Avoid void* except in low-level code; [§7.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch07.html#ch7lev2sec2sec1).

1. Use const pointers and const references to express immutability in interfaces; [§7.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch07.html#ch7lev1sec5).

1. Prefer references to pointers as arguments, except where “no object” is a reasonable option; [§7.7.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch07.html#ch7lev2sec7sec4).

## 8. Structures, Unions, and Enumerations

1. When compactness of data is important, lay out structure data members with larger members before smaller ones; [§8.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch08.html#ch8lev2sec2sec1).

1. Use bit-fields to represent hardware-imposed data layouts; [§8.2.7](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch08.html#ch8lev2sec2sec7).

1. Don’t naively try to optimize memory consumption by packing several values into a single byte; [§8.2.7](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch08.html#ch8lev2sec2sec7).

1. Use unions to save space (represent alternatives) and never for type conversion; [§8.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch08.html#ch8lev1sec3).

1. Use enumerations to represent sets of named constants; [§8.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch08.html#ch8lev1sec4).

1. Prefer enum classes over “plain” enums to minimize surprises; [§8.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch08.html#ch8lev1sec4).

1. Define operations on enumerations for safe and simple use; [§8.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch08.html#ch8lev2sec4sec1).

## 9. Statements

1. Don’t declare a variable until you have a value to initialize it with; [§9.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch09.html#ch9lev1sec3), [§9.4.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch09.html#ch9lev2sec4sec3), [§9.5.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch09.html#ch9lev2sec5sec2).

1. Prefer a switch-statement to an if-statement when there is a choice; [§9.4.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch09.html#ch9lev2sec4sec2).

1. Prefer a range-for-statement to a for-statement when there is a choice; [§9.5.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch09.html#ch9lev2sec5sec1).

1. Prefer a for-statement to a while-statement when there is an obvious loop variable; [§9.5.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch09.html#ch9lev2sec5sec2).

1. Prefer a while-statement to a for-statement when there is no obvious loop variable; [§9.5.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch09.html#ch9lev2sec5sec3).

1. Avoid do-statements; [§9.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch09.html#ch9lev1sec5).

1. Avoid goto; [§9.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch09.html#ch9lev1sec6).

1. Keep comments crisp; [§9.7](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch09.html#ch9lev1sec7).

1. Don’t say in comments what can be clearly stated in code; [§9.7](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch09.html#ch9lev1sec7).

1. State intent in comments; [§9.7](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch09.html#ch9lev1sec7).

1. Maintain a consistent indentation style; [§9.7](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch09.html#ch9lev1sec7).

## 10. Expressions

1. Prefer the standard library to other libraries and to “handcrafted code”; [§10.2.8](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch10.html#ch10lev2sec2sec8).

1. Use character-level input only when you have to; [§10.2.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch10.html#ch10lev2sec2sec3).

1. When reading, always consider ill-formed input; [§10.2.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch10.html#ch10lev2sec2sec3).

1. Prefer suitable abstractions (classes, algorithms, etc.) to direct use of language features (e.g., ints, statements); [§10.2.8](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch10.html#ch10lev2sec2sec8).

1. Avoid complicated expressions; [§10.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch10.html#ch10lev2sec3sec3).

1. If in doubt about operator precedence, parenthesize; [§10.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch10.html#ch10lev2sec3sec3).

1. Avoid expressions with undefined order of evaluation; [§10.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch10.html#ch10lev2sec3sec2).

1. Avoid narrowing conversions; [§10.5.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch10.html#ch10lev2sec5sec2).

1. Define symbolic constants to avoid “magic constants”; [§10.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch10.html#ch10lev2sec4sec1).

## 11. Select Operations

1. Prefer prefix ++ over suffix ++; [§11.1.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch11.html#ch11lev2sec1sec4).

1. Use resource handles to avoid leaks, premature deletion, and double deletion; [§11.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch11.html#ch11lev2sec2sec1).

1. Don’t put objects on the free store if you don’t have to; prefer scoped variables; [§11.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch11.html#ch11lev2sec2sec1).

1. Avoid “naked new” and “naked delete”; [§11.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch11.html#ch11lev2sec2sec1).

1. Use RAII; [§11.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch11.html#ch11lev2sec2sec1).

1. Prefer a named function object to a lambda if the operation requires comments; [§11.4.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch11.html#ch11lev2sec4sec2).

1. Prefer a named function object to a lambda if the operation is generally useful; [§11.4.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch11.html#ch11lev2sec4sec2).

1. Keep lambdas short; [§11.4.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch11.html#ch11lev2sec4sec2).

1. For maintainability and correctness, be careful about capture by reference; [§11.4.3.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch11.html#ch11lev3sec4sec3-1).

1. Let the compiler deduce the return type of a lambda; [§11.4.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch11.html#ch11lev2sec4sec4).

1. Use the T{e} notation for construction; [§11.5.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch11.html#ch11lev2sec5sec1).

1. Avoid explicit type conversion (casts); [§11.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch11.html#ch11lev1sec5).

1. When explicit type conversion is necessary, prefer a named cast; [§11.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch11.html#ch11lev1sec5).

1. Consider using a run-time checked cast, such as narrow_cast\<>(), for conversion between numeric types; [§11.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch11.html#ch11lev1sec5).

## 12. Functions

1. “Package” meaningful operations as carefully named functions; [§12.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch12.html#ch12lev1sec1).

1. A function should perform a single logical operation; [§12.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch12.html#ch12lev1sec1).

1. Keep functions short; [§12.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch12.html#ch12lev1sec1).

1. Don’t return pointers or references to local variables; [§12.1.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch12.html#ch12lev2sec1sec4).

1. If a function may have to be evaluated at compile time, declare it constexpr; [§12.1.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch12.html#ch12lev2sec1sec6).

1. If a function cannot return, mark it [[noreturn]]; [§12.1.7](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch12.html#ch12lev2sec1sec7).

1. Use pass-by-value for small objects; [§12.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch12.html#ch12lev2sec2sec1).

1. Use pass-by-const-reference to pass large values that you don’t need to modify; [§12.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch12.html#ch12lev2sec2sec1).

1. Return a result as a return value rather than modifying an object through an argument; [§12.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch12.html#ch12lev2sec2sec1).

1. Use rvalue references to implement move and forwarding; [§12.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch12.html#ch12lev2sec2sec1).

1. Pass a pointer if “no object” is a valid alternative (and represent “no object” by nullptr); [§12.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch12.html#ch12lev2sec2sec1).

1. Use pass-by-non-const-reference only if you have to; [§12.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch12.html#ch12lev2sec2sec1).

1. Use const extensively and consistently; [§12.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch12.html#ch12lev2sec2sec1).

1. Assume that a char* or a const char* argument points to a C-style string; [§12.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch12.html#ch12lev2sec2sec2).

1. Avoid passing arrays as pointers; [§12.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch12.html#ch12lev2sec2sec2).

1. Pass a homogeneous list of unknown length as an initializer_list\<T> (or as some other container); [§12.2.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch12.html#ch12lev2sec2sec3).

1. Avoid unspecified numbers of arguments (...); [§12.2.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch12.html#ch12lev2sec2sec4).

1. Use overloading when functions perform conceptually the same task on different types; [§12.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch12.html#ch12lev1sec3).

1. When overloading on integers, provide functions to eliminate common ambiguities; [§12.3.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch12.html#ch12lev2sec3sec5).

1. Specify preconditions and postconditions for your functions; [§12.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch12.html#ch12lev1sec4).

1. Prefer function objects (including lambdas) and virtual functions to pointers to functions; [§12.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch12.html#ch12lev1sec5).

1. Avoid macros; [§12.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch12.html#ch12lev1sec6).

1. If you must use macros, use ugly names with lots of capital letters; [§12.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch12.html#ch12lev1sec6).

## 13. Exception Handling

1. Develop an error-handling strategy early in a design; [§13.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev1sec1).

1. Throw an exception to indicate that you cannot perform an assigned task; [§13.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev2sec1sec1).

1. Use exceptions for error handling; [§13.1.4.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev3sec1sec4-2).

1. Use purpose-designed user-defined types as exceptions (not built-in types); [§13.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev2sec1sec1).

1. If you for some reason cannot use exceptions, mimic them; [§13.1.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev2sec1sec5).

1. Use hierarchical error handling; [§13.1.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev2sec1sec6).

1. Keep the individual parts of error handling simple; [§13.1.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev2sec1sec6).

1. Don’t try to catch every exception in every function; [§13.1.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev2sec1sec6).

1. Always provide the basic guarantee; [§13.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev1sec2), [§13.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev1sec6).

1. Provide the strong guarantee unless there is a reason not to; [§13.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev1sec2), [§13.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev1sec6).

1. Let a constructor establish an invariant, and throw if it cannot; [§13.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev1sec2).

1. Release locally owned resources before throwing an exception; [§13.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev1sec2).

1. Be sure that every resource acquired in a constructor is released when throwing an exception in that constructor; [§13.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev1sec3).

1. Don’t use exceptions where more local control structures will suffice; [§13.1.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev2sec1sec4).

1. Use the “Resource Acquisition Is Initialization” technique to manage resources; [§13.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev1sec3).

1. Minimize the use of try-blocks; [§13.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev1sec3).

1. Not every program needs to be exception-safe; [§13.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev1sec1).

1. Use “Resource Acquisition Is Initialization” and exception handlers to maintain invariants; [§13.5.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev3sec5sec2-2).

1. Prefer proper resource handles to the less structured finally; [§13.3.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev2sec3sec1).

1. Design your error-handling strategy around invariants; [§13.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev1sec4).

1. What can be checked at compile time is usually best checked at compile time (using static_assert); [§13.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev1sec4).

1. Design your error-handling strategy to allow for different levels of checking/enforcement; [§13.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev1sec4).

1. If your function may not throw, declare it noexcept; [§13.5.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev3sec5sec1-1)

1. Don’t use exception specification; [§13.5.1.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev3sec5sec1-3).

1. Catch exceptions that may be part of a hierarchy by reference; [§13.5.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev2sec5sec2).

1. Don’t assume that every exception is derived from class exception; [§13.5.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev3sec5sec2-2).

1. Have main() catch and report all exceptions; [§13.5.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev3sec5sec2-2), [§13.5.2.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev3sec5sec2-4).

1. Don’t destroy information before you have its replacement ready; [§13.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev1sec6).

1. Leave operands in valid states before throwing an exception from an assignment; [§13.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev1sec2).

1. Never let an exception escape from a destructor; [§13.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev1sec2).

1. Keep ordinary code and error-handling code separate; [§13.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev2sec1sec1), [§13.1.4.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev3sec1sec4-2).

1. Beware of memory leaks caused by memory allocated by new not being released in case of an exception; [§13.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev1sec3).

1. Assume that every exception that can be thrown by a function will be thrown; [§13.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev1sec2).

1. A library shouldn’t unilaterally terminate a program. Instead, throw an exception and let a caller decide; [§13.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev1sec4).

1. A library shouldn’t produce diagnostic output aimed at an end user. Instead, throw an exception and let a caller decide; [§13.1.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev2sec1sec3).

## 14. Namespaces

1. Use namespaces to express logical structure; [§14.3.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch14.html#ch14lev2sec3sec1).

1. Place every nonlocal name, except main(), in some namespace; [§14.3.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch14.html#ch14lev2sec3sec1).

1. Design a namespace so that you can conveniently use it without accidentally gaining access to unrelated namespaces; [§14.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch14.html#ch14lev2sec3sec3).

1. Avoid very short names for namespaces; [§14.4.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch14.html#ch14lev2sec4sec2).

1. If necessary, use namespace aliases to abbreviate long namespace names; [§14.4.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch14.html#ch14lev2sec4sec2).

1. Avoid placing heavy notational burdens on users of your namespaces; [§14.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch14.html#ch14lev2sec2sec2), [§14.2.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch14.html#ch14lev2sec2sec3).

1. Use separate namespaces for interfaces and implementations; [§14.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch14.html#ch14lev2sec3sec3).

1. Use the Namespace::member notation when defining namespace members; [§14.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch14.html#ch14lev1sec4).

1. Use inline namespaces to support versioning; [§14.4.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch14.html#ch14lev2sec4sec6).

1. Use using-directives for transition, for foundational libraries (such as std), or within a local scope; [§14.4.9](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch14.html#ch14lev2sec4sec9).

1. Don’t put a using-directive in a header file; [§14.2.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch14.html#ch14lev2sec2sec3).

## 15. Source Files and Programs

1. Use header files to represent interfaces and to emphasize logical structure; [§15.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch15.html#ch15lev1sec1), [§15.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch15.html#ch15lev2sec3sec2).

1. #include a header in the source file that implements its functions; [§15.3.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch15.html#ch15lev2sec3sec1).

1. Don’t define global entities with the same name and similar-but-different meanings in different translation units; [§15.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch15.html#ch15lev1sec2).

1. Avoid non-inline function definitions in headers; [§15.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch15.html#ch15lev2sec2sec2).

1. Use #include only at global scope and in namespaces; [§15.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch15.html#ch15lev2sec2sec2).

1. #include only complete declarations; [§15.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch15.html#ch15lev2sec2sec2).

1. Use include guards; [§15.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch15.html#ch15lev2sec3sec3).

1. #include C headers in namespaces to avoid global names; [§14.4.9](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch14.html#ch14lev2sec4sec9), [§15.2.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch15.html#ch15lev2sec2sec4).

1. Make headers self-contained; [§15.2.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch15.html#ch15lev2sec2sec3).

1. Distinguish between users’ interfaces and implementers’ interfaces; [§15.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch15.html#ch15lev2sec3sec2).

1. Distinguish between average users’ interfaces and expert users’ interfaces; [§15.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch15.html#ch15lev2sec3sec2).

1. Avoid nonlocal objects that require run-time initialization in code intended for use as part of non-C++ programs; [§15.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch15.html#ch15lev2sec4sec1).

## 16. Classes

1. Represent concepts as classes; [§16.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch16.html#ch16lev1sec1).

1. Separate the interface of a class from its implementation; [§16.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch16.html#ch16lev1sec1).

1. Use public data (structs) only when it really is just data and no invariant is meaningful for the data members; [§16.2.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch16.html#ch16lev2sec2sec4).

1. Define a constructor to handle initialization of objects; [§16.2.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch16.html#ch16lev2sec2sec5).

1. By default declare single-argument constructors explicit; [§16.2.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch16.html#ch16lev2sec2sec6).

1. Declare a member function that does not modify the state of its object const; [§16.2.9](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch16.html#ch16lev2sec2sec9).

1. A concrete type is the simplest kind of class. Where applicable, prefer a concrete type over more complicated classes and over plain data structures; [§16.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch16.html#ch16lev1sec3).

1. Make a function a member only if it needs direct access to the representation of a class; [§16.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch16.html#ch16lev2sec3sec2).

1. Use a namespace to make the association between a class and its helper functions explicit; [§16.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch16.html#ch16lev2sec3sec2).

1. Use in-class initializers to avoid repetition in constructors; [§16.2.7](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch16.html#ch16lev2sec2sec7).

1. Make a function that needs access to the representation of a class but needn’t be called for a specific object a static member function; [§16.2.12](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch16.html#ch16lev2sec2sec12).

## 17. Construction, Cleanup, Copy, and Move

1. Design constructors, assignments, and the destructor as a matched set of operations; [§17.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch17.html#ch17lev1sec1).

1. Use a constructor to establish an invariant for a class; [§17.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch17.html#ch17lev2sec2sec1).

1. If a constructor acquires a resource, its class needs a destructor to release the resource; [§17.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch17.html#ch17lev2sec2sec2).

1. If a class has a virtual function, it needs a virtual destructor; [§17.2.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch17.html#ch17lev2sec2sec5).

1. If a class does not have a constructor, it can be initialized by memberwise initialization; [§17.3.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch17.html#ch17lev2sec3sec1).

1. Prefer {} initialization over = and () initialization; [§17.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch17.html#ch17lev2sec3sec2).

1. Give a class a default constructor if and only if there is a “natural” default value; [§17.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch17.html#ch17lev2sec3sec3).

1. If a class is a container, give it an initializer-list constructor; [§17.3.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch17.html#ch17lev2sec3sec4).

1. Initialize members and bases in their order of declaration; [§17.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch17.html#ch17lev2sec4sec1).

1. If a class has a reference member, it probably needs copy operations (copy constructor and copy assignment); [§17.4.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch17.html#ch17lev3sec4sec1-1).

1. Prefer member initialization over assignment in a constructor; [§17.4.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch17.html#ch17lev3sec4sec1-1).

1. Use in-class initializers to provide default values; [§17.4.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch17.html#ch17lev2sec4sec4).

1. If a class is a resource handle, it probably needs copy and move operations; [§17.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch17.html#ch17lev1sec5).

1. When writing a copy constructor, be careful to copy every element that needs to be copied (beware of default initializers); [§17.5.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch17.html#ch17lev3sec5sec1-1).

1. A copy operation should provide equivalence and independence; [§17.5.1.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch17.html#ch17lev3sec5sec1-3).

1. Beware of entangled data structures; [§17.5.1.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch17.html#ch17lev3sec5sec1-3).

1. Prefer move semantics and copy-on-write to shallow copy; [§17.5.1.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch17.html#ch17lev3sec5sec1-3).

1. If a class is used as a base class, protect against slicing; [§17.5.1.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch17.html#ch17lev3sec5sec1-4).

1. If a class needs a copy operation or a destructor, it probably needs a constructor, a destructor, a copy assignment, and a copy constructor; [§17.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch17.html#ch17lev1sec6).

1. If a class has a pointer member, it probably needs a destructor and non-default copy operations; [§17.6.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch17.html#ch17lev3sec6sec3-3).

1. If a class is a resource handle, it needs a constructor, a destructor, and non-default copy operations; [§17.6.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch17.html#ch17lev3sec6sec3-3).

1. If a default constructor, assignment, or destructor is appropriate, let the compiler generate it (don’t rewrite it yourself); [§17.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch17.html#ch17lev1sec6).

1. Be explicit about your invariants; use constructors to establish them and assignments to maintain them; [§17.6.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch17.html#ch17lev3sec6sec3-2).

1. Make sure that copy assignments are safe for self-assignment; [§17.5.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch17.html#ch17lev2sec5sec1).

1. When adding a new member to a class, check to see if there are user-defined constructors that need to be updated to initialize the member; [§17.5.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch17.html#ch17lev2sec5sec1).

## 18. Operator Overloading

1. Define operators primarily to mimic conventional usage; [§18.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch18.html#ch18lev1sec1).

1. Redefine or prohibit copying if the default is not appropriate for a type; [§18.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch18.html#ch18lev2sec2sec2).

1. For large operands, use const reference argument types; [§18.2.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch18.html#ch18lev2sec2sec4).

1. For large results, use a move constructor; [§18.2.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch18.html#ch18lev2sec2sec4).

1. Prefer member functions over nonmembers for operations that need access to the representation; [§18.3.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch18.html#ch18lev2sec3sec1).

1. Prefer nonmember functions over members for operations that do not need access to the representation; [§18.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch18.html#ch18lev2sec3sec2).

1. Use namespaces to associate helper functions with “their” class; [§18.2.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch18.html#ch18lev2sec2sec5).

1. Use nonmember functions for symmetric operators; [§18.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch18.html#ch18lev2sec3sec2).

1. Use member functions to express operators that require an lvalue as their left-hand operand; [§18.3.3.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch18.html#ch18lev3sec3sec3-1).

1. Use user-defined literals to mimic conventional notation; [§18.3.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch18.html#ch18lev2sec3sec4).

1. Provide “set() and get() functions” for a data member only if the fundamental semantics of a class require them; [§18.3.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch18.html#ch18lev2sec3sec5).

1. Be cautious about introducing implicit conversions; [§18.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch18.html#ch18lev1sec4).

1. Avoid value-destroying (“narrowing”) conversions; [§18.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch18.html#ch18lev2sec4sec1).

1. Do not define the same conversion as both a constructor and a conversion operator; [§18.4.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch18.html#ch18lev2sec4sec3).

## 19. Special Operators

1. Use operator\[]() for subscripting and for selection based on a single value; [§19.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch19.html#ch19lev2sec2sec1).

1. Use operator()() for call semantics, for subscripting, and for selection based on multiple values; [§19.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch19.html#ch19lev2sec2sec2).

1. Use operator–>() to dereference “smart pointers”; [§19.2.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch19.html#ch19lev2sec2sec3).

1. Prefer prefix ++ over suffix ++; [§19.2.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch19.html#ch19lev2sec2sec4).

1. Define the global operator new() and operator delete() only if you really have to; [§19.2.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch19.html#ch19lev2sec2sec5).

1. Define member operator new() and member operator delete() to control allocation and deallocation of objects of a specific class or hierarchy of classes; [§19.2.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch19.html#ch19lev2sec2sec5).

1. Use user-defined literals to mimic conventional notation; [§19.2.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch19.html#ch19lev2sec2sec6).

1. Place literal operators in separate namespaces to allow selective use; [§19.2.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch19.html#ch19lev2sec2sec6).

1. For nonspecialized uses, prefer the standard string (Chapter 36) to the result of your own exercises; [§19.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch19.html#ch19lev1sec3).

1. Use a friend function if you need a nonmember function to have access to the representation of a class (e.g., to improve notation or to access the representation of two classes); [§19.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch19.html#ch19lev1sec4).

1. Prefer member functions to friend functions for granting access to the implementation of a class; [§19.4.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch19.html#ch19lev2sec4sec2).

## 20. Derived Classes

1. Avoid type fields; [§20.3.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch20.html#ch20lev2sec3sec1).

1. Access polymorphic objects through pointers and references; [§20.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch20.html#ch20lev2sec3sec2).

1. Use abstract classes to focus design on the provision of clean interfaces; [§20.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch20.html#ch20lev1sec4).

1. Use override to make overriding explicit in large class hierarchies; [§20.3.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch20.html#ch20lev3sec3sec4-1).

1. Use final only sparingly; [§20.3.4.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch20.html#ch20lev3sec3sec4-2).

1. Use abstract classes to specify interfaces; [§20.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch20.html#ch20lev1sec4).

1. Use abstract classes to keep implementation details out of interfaces; [§20.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch20.html#ch20lev1sec4).

1. A class with a virtual function should have a virtual destructor; [§20.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch20.html#ch20lev1sec4).

1. An abstract class typically doesn’t need a constructor; [§20.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch20.html#ch20lev1sec4).

1. Prefer private members for implementation details; [§20.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch20.html#ch20lev1sec5).

1. Prefer public members for interfaces; [§20.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch20.html#ch20lev1sec5).

1. Use protected members only carefully when really needed; [§20.5.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch20.html#ch20lev3sec5sec1-1).

1. Don’t declare data members protected; [§20.5.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch20.html#ch20lev3sec5sec1-1).

## 21. Class Hierarchies

1. Use unique_ptr or shared_ptr to avoid forgetting to delete objects created using new; [§21.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch21.html#ch21lev2sec2sec1).

1. Avoid data members in base classes intended as interfaces; [§21.2.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch21.html#ch21lev3sec2sec1-1).

1. Use abstract classes to express interfaces; [§21.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch21.html#ch21lev2sec2sec2).

1. Give an abstract class a virtual destructor to ensure proper cleanup; [§21.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch21.html#ch21lev2sec2sec2).

1. Use override to make overriding explicit in large class hierarchies; [§21.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch21.html#ch21lev2sec2sec2).

1. Use abstract classes to support interface inheritance; [§21.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch21.html#ch21lev2sec2sec2).

1. Use base classes with data members to support implementation inheritance; [§21.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch21.html#ch21lev2sec2sec2).

1. Use ordinary multiple inheritance to express a union of features; [§21.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch21.html#ch21lev1sec3).

1. Use multiple inheritance to separate implementation from interface; [§21.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch21.html#ch21lev1sec3).

1. Use a virtual base to represent something common to some, but not all, classes in a hierarchy; [§21.3.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch21.html#ch21lev2sec3sec5).

## 22. Run-Time Type Information

1. Use virtual functions to ensure that the same operation is performed independently of which interface is used for an object; [§22.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch22.html#ch22lev1sec1).

1. Use dynamic_cast where class hierarchy navigation is unavoidable; [§22.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch22.html#ch22lev1sec2).

1. Use dynamic_cast for type-safe explicit navigation of a class hierarchy; [§22.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch22.html#ch22lev2sec2sec1).

1. Use dynamic_cast to a reference type when failure to find the required class is considered a failure; [§22.2.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch22.html#ch22lev3sec2sec1-1).

1. Use dynamic_cast to a pointer type when failure to find the required class is considered a valid alternative; [§22.2.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch22.html#ch22lev3sec2sec1-1).

1. Use double dispatch or the visitor pattern to express operations on two dynamic types (unless you need an optimized lookup); [§22.3.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch22.html#ch22lev2sec3sec1).

1. Don’t call virtual functions during construction or destruction; [§22.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch22.html#ch22lev1sec4).

1. Use typeid to implement extended type information; [§22.5.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch22.html#ch22lev2sec5sec1).

1. Use typeid to find the type of an object (and not to find an interface to an object); [§22.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch22.html#ch22lev1sec5).

1. Prefer virtual functions to repeated switch-statements based on typeid or dynamic_cast; [§22.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch22.html#ch22lev1sec6).

## 23. Templates

1. Use templates to express algorithms that apply to many argument types; [§23.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch23.html#ch23lev1sec1).

1. Use templates to express containers; [§23.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch23.html#ch23lev1sec2).

1. Note that template\<class T> and template\<typename T> are synonymous; [§23.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch23.html#ch23lev1sec2).

1. When defining a template, first design and debug a non-template version; later generalize by adding parameters; [§23.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch23.html#ch23lev2sec2sec1).

1. Templates are type-safe, but checking happens too late; [§23.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch23.html#ch23lev1sec3).

1. When designing a template, carefully consider the concepts (requirements) assumed for its template arguments; [§23.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch23.html#ch23lev1sec3).

1. If a class template should be copyable, give it a non-template copy constructor and a non-template copy assignment; [§23.4.6.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch23.html#ch23lev3sec4sec6-1).

1. If a class template should be movable, give it a non-template move constructor and a non-template move assignment; [§23.4.6.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch23.html#ch23lev3sec4sec6-1).

1. A virtual function member cannot be a template member function; [§23.4.6.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch23.html#ch23lev3sec4sec6-2).

1. Define a type as a member of a template only if it depends on all the class template’s arguments; [§23.4.6.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch23.html#ch23lev3sec4sec6-3).

1. Use function templates to deduce class template argument types; [§23.5.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch23.html#ch23lev2sec5sec1).

1. Overload function templates to get the same semantics for a variety of argument types; [§23.5.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch23.html#ch23lev2sec5sec3).

1. Use argument substitution failure to provide just the right set of functions for a program; [§23.5.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch23.html#ch23lev3sec5sec3-2).

1. Use template aliases to simplify notation and hide implementation details; [§23.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch23.html#ch23lev1sec6).

1. There is no separate compilation of templates: #include template definitions in every translation unit that uses them; [§23.7](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch23.html#ch23lev1sec7).

1. Use ordinary functions as interfaces to code that cannot deal with templates; [§23.7.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch23.html#ch23lev2sec7sec1).

1. Separately compile large templates and templates with nontrivial context dependencies; [§23.7](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch23.html#ch23lev1sec7).

## 24. Generic Programming

1. A template can pass argument types without loss of information; [§24.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch24.html#ch24lev1sec1).

1. Templates provide a general mechanism for compile-time programming; [§24.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch24.html#ch24lev1sec1).

1. Templates provide compile-time “duck typing”; [§24.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch24.html#ch24lev1sec1).

1. Design generic algorithms by “lifting” from concrete examples; [§24.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch24.html#ch24lev1sec2).

1. Generalize algorithms by specifying template argument requirements in terms of concepts; [§24.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch24.html#ch24lev1sec3).

1. Do not give unconventional meaning to conventional notation; [§24.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch24.html#ch24lev1sec3).

1. Use concepts as a design tool; [§24.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch24.html#ch24lev1sec3).

1. Aim for “plug compatibility” among algorithms and argument type by using common and regular template argument requirements; [§24.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch24.html#ch24lev1sec3).

1. Discover a concept by minimizing an algorithm’s requirements on its template arguments and then generalizing for wider use; [§24.3.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch24.html#ch24lev2sec3sec1).

1. A concept is not just a description of the needs of a particular implementation of an algorithm; [§24.3.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch24.html#ch24lev2sec3sec1).

1. If possible, choose a concept from a list of well-known concepts; [§24.3.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch24.html#ch24lev2sec3sec1), [§24.4.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch24.html#ch24lev2sec4sec4).

1. The default concept for a template argument is Regular; [§24.3.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch24.html#ch24lev2sec3sec1).

1. Not all template argument types are Regular; [§24.3.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch24.html#ch24lev2sec3sec1).

1. A concept requires a semantic aspect; it is not primarily a syntactic notion; [§24.3.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch24.html#ch24lev2sec3sec1), [§24.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch24.html#ch24lev2sec3sec2), [§24.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch24.html#ch24lev2sec4sec1).

1. Make concepts concrete in code; [§24.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch24.html#ch24lev1sec4).

1. Express concepts as compile-time predicates (constexpr functions) and test them using static_assert() or enable_if\<>; [§24.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch24.html#ch24lev1sec4).

1. Use axioms as a design tool; [§24.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch24.html#ch24lev2sec4sec1).

1. Use axioms as a guide for testing; [§24.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch24.html#ch24lev2sec4sec1).

1. Some concepts involve two or more template arguments; [§24.4.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch24.html#ch24lev2sec4sec2).

1. Concepts are not just types of types; [§24.4.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch24.html#ch24lev2sec4sec2).

1. Concepts can involve numeric values; [§24.4.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch24.html#ch24lev2sec4sec3).

1. Use concepts as a guide for testing template definitions; [§24.4.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch24.html#ch24lev2sec4sec5).

## 25. Specialization

1. Use templates to improve type safety; [§25.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch25.html#ch25lev1sec1).

1. Use templates to raise the level of abstraction of code; [§25.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch25.html#ch25lev1sec1).

1. Use templates to provide flexible and efficient parameterization of types and algorithms; [§25.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch25.html#ch25lev1sec1).

1. Remember that value template arguments must be compile-time constants; [§25.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch25.html#ch25lev2sec2sec2).

1. Use function objects as type arguments to parameterize types and algorithms with “policies”; [§25.2.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch25.html#ch25lev2sec2sec3).

1. Use default template arguments to provide simple notation for simple uses; [§25.2.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch25.html#ch25lev2sec2sec5).

1. Specialize templates for irregular types (such as arrays); [§25.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch25.html#ch25lev1sec3).

1. Specialize templates to optimize for important cases; [§25.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch25.html#ch25lev1sec3).

1. Define the primary template before any specialization; [§25.3.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch25.html#ch25lev3sec3sec1-1).

1. A specialization must be in scope for every use; [§25.3.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch25.html#ch25lev3sec3sec1-1).

## 26. Instantiation

1. Let the compiler/implementation generate specializations as needed; [§26.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch26.html#ch26lev2sec2sec1).

1. Explicitly instantiate if you need exact control of the instantiation environment; [§26.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch26.html#ch26lev2sec2sec2).

1. Explicitly instantiate if you optimize the time needed to generate specializations; [§26.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch26.html#ch26lev2sec2sec2).

1. Avoid subtle context dependencies in a template definition; [§26.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch26.html#ch26lev1sec3).

1. Names must be in scope when used in a template definition or findable through argument-dependent lookup (ADL); [§26.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch26.html#ch26lev1sec3), [§26.3.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch26.html#ch26lev2sec3sec5).

1. Keep the binding context unchanged between instantiation points; [§26.3.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch26.html#ch26lev2sec3sec4).

1. Avoid fully general templates that can be found by ADL; [§26.3.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch26.html#ch26lev2sec3sec6).

1. Use concepts and/or static_assert to avoid using inappropriate templates; [§26.3.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch26.html#ch26lev2sec3sec6).

1. Use using-declarations to limit the reach of ADL; [§26.3.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch26.html#ch26lev2sec3sec6).

1. Qualify names from a template base class with –> or T:: as appropriate; [§26.3.7](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch26.html#ch26lev2sec3sec7).

## 27. Templates and Hierarchies

1. When having to express a general idea in code, consider whether to represent it as a template or as a class hierachy; [§27.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch27.html#ch27lev1sec1).

1. A template usually provides common code for a variety of arguments; [§27.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch27.html#ch27lev1sec1).

1. An abstract class can completely hide implementation details from users; [§27.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch27.html#ch27lev1sec1).

1. Irregular implementations are usually best represented as derived classes; [§27.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch27.html#ch27lev1sec2).

1. If explicit use of free store is undesirable, templates have an advantage over class hierarchies; [§27.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch27.html#ch27lev1sec2).

1. Templates have an advantage over abstract classes where inlining is important; [§27.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch27.html#ch27lev1sec2).

1. Template interfaces are easily expressed in terms of template argument types; [§27.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch27.html#ch27lev1sec2).

1. If run-time resolution is needed, class hierarchies are necessary; [§27.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch27.html#ch27lev1sec2).

1. The combination of templates and class hierarchies is often superior to either without the other; [§27.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch27.html#ch27lev1sec2).

1. Think of templates as type generators (and function generators); [§27.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch27.html#ch27lev2sec2sec1).

1. There is no default relation between two classes generated from the same template; [§27.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch27.html#ch27lev2sec2sec1).

1. Do not mix class hierarchies and arrays; [§27.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch27.html#ch27lev2sec2sec1).

1. Do not naively templatize large class hierarchies; [§27.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch27.html#ch27lev1sec3).

1. A template can be used to provide a type-safe interface to a single (weakly typed) implementation; [§27.3.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch27.html#ch27lev2sec3sec1).

1. Templates can be used to compose type-safe and compact data structures; [§27.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch27.html#ch27lev2sec4sec1).

1. Templates can be used to linearize a class hierarchy (minimizing space and access time); [§27.4.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch27.html#ch27lev2sec4sec2).

## 28. Metaprogramming

1. Use metaprogramming to improve type safety; [§28.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch28.html#ch28lev1sec1).

1. Use metaprogramming to improve performance by moving computation to compile time; [§28.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch28.html#ch28lev1sec1).

1. Avoid using metaprogramming to an extent where it significantly slows down compilation; [§28.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch28.html#ch28lev1sec1).

1. Think in terms of compile-time evaluation and type functions; [§28.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch28.html#ch28lev1sec2).

1. Use template aliases as the interfaces to type functions returning types; [§28.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch28.html#ch28lev2sec2sec1).

1. Use constexpr functions as the interfaces to type functions returning (non-type) values; [§28.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch28.html#ch28lev2sec2sec2).

1. Use traits to nonintrusively associate properties with types; [§28.2.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch28.html#ch28lev2sec2sec4).

1. Use Conditional to choose between two types; [§28.3.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch28.html#ch28lev3sec3sec1-1).

1. Use Select to choose among several alternative types; [§28.3.1.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch28.html#ch28lev3sec3sec1-3).

1. Use recursion to express compile-time iteration; [§28.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch28.html#ch28lev2sec3sec2).

1. Use metaprogramming for tasks that cannot be done well at run time; [§28.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch28.html#ch28lev2sec3sec3).

1. Use Enable_if to selectively declare function templates; [§28.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch28.html#ch28lev1sec4).

1. Concepts are among the most useful predicates to use with Enable_if; [§28.4.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch28.html#ch28lev2sec4sec3).

1. Use variadic templates when you need a function that takes a variable number of arguments of a variety of types; [§28.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch28.html#ch28lev1sec6).

1. Don’t use variadic templates for homogeneous argument lists (prefer initializer lists for that); [§28.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch28.html#ch28lev1sec6).

1. Use variadic templates and std::forward() where forwarding is needed; [§28.6.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch28.html#ch28lev2sec6sec3).

1. Use simple metaprogramming to implement efficient and elegant unit systems (for fine-grained type checking); [§28.7](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch28.html#ch28lev1sec7).

1. Use user-defined literals to simplify the use of units; [§28.7](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch28.html#ch28lev1sec7).

## 29. A Matrix Design

1. List basic use cases; [§29.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch29.html#ch29lev2sec1sec1).

1. Always provide input and output operations to simplify simple testing (e.g., unit testing); [§29.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch29.html#ch29lev2sec1sec1).

1. Carefully list the properties a program, class, or library ideally should have; [§29.1.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch29.html#ch29lev2sec1sec2).

1. List the properties of a program, class, or library that are considered beyond the scope of the project; [§29.1.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch29.html#ch29lev2sec1sec2).

1. When designing a container template, carefully consider the requirements on the element type; [§29.1.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch29.html#ch29lev2sec1sec2).

1. Consider how the design might accommodate run-time checking (e.g., for debugging); [§29.1.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch29.html#ch29lev2sec1sec2).

1. If possible, design a class to mimic existing professional notation and semantics; [§29.1.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch29.html#ch29lev2sec1sec2).

1. Make sure that the design does not leak resources (e.g., have a unique owner for each resource and use RAII); [§29.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch29.html#ch29lev1sec2).

1. Consider how a class can be constructed and copied; [§29.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch29.html#ch29lev2sec1sec1).

1. Provide complete, flexible, efficient, and semantically meaningful access to elements; [§29.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch29.html#ch29lev2sec2sec2), [§29.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch29.html#ch29lev1sec3).

1. Place implementation details in their own _impl namespace; [§29.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch29.html#ch29lev1sec4).

1. Provide common operations that do not require direct access to the representation as helper functions; [§29.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch29.html#ch29lev2sec3sec2), [§29.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch29.html#ch29lev2sec3sec3).

1. For fast access, keep data compact and use accessor objects to provide necessary nontrivial access operations; [§29.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch29.html#ch29lev2sec4sec1),[§29.4.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch29.html#ch29lev2sec4sec2), [§29.4.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch29.html#ch29lev2sec4sec3).

1. The structure of data can often be expressed as nested initializer lists; [§29.4.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch29.html#ch29lev2sec4sec4).

1. When dealing with numbers, aways consider “end cases,” such as zero and “many”; [§29.4.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch29.html#ch29lev2sec4sec6).

1. In addition to unit testing and testing that the code meets its requirements, test the design through examples of real use; [§29.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch29.html#ch29lev1sec5).

1. Consider how the design might accommodate unusually stringent performance requirements; [§29.5.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch29.html#ch29lev2sec5sec4)

## 30. Standard-Library Overview

1. Use standard-library facilities to maintain portability; [§30.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch30.html#ch30lev1sec1), [§30.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch30.html#ch30lev2sec1sec1).

1. Use standard-library facilities to minimize maintenance costs; [§30.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch30.html#ch30lev1sec1).

1. Use standard-library facilities as a base for more extensive and more specialized libraries; [§30.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch30.html#ch30lev2sec1sec1).

1. Use standard-library facilities as a model for flexible, widely usable software; [§30.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch30.html#ch30lev2sec1sec1).

1. The standard-library facilities are defined in namespace std and found in standard-library headers; [§30.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch30.html#ch30lev1sec2).

1. A C standard-library header X.h is presented as a C++ standard-library header in \<cX>; [§30.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch30.html#ch30lev1sec2).

1. Do not try to use a standard-library facility without #includeing its header; [§30.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch30.html#ch30lev1sec2).

1. To use a range-for on a built-in array, #include\<iterator>; [§30.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch30.html#ch30lev2sec3sec2).

1. Prefer exception-based error handling over return-code-based error handling; [§30.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch30.html#ch30lev1sec4).

1. Always catch exception& (for standard-library and language support exceptions) and ... (for unexpected exceptions); [§30.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch30.html#ch30lev2sec4sec1).

1. The standard-library exception hierarchy can be (but does not have to be) used for a user’s own exceptions; [§30.4.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch30.html#ch30lev3sec4sec1-1).

1. Call terminate() in case of serious trouble; [§30.4.1.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch30.html#ch30lev3sec4sec1-3).

1. Use static_assert() and assert() extensively; [§30.4.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch30.html#ch30lev2sec4sec2).

1. Do not assume that assert() is always evaluated; [§30.4.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch30.html#ch30lev2sec4sec2).

1. If you can’t use exceptions, consider \<system_error>; [§30.4.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch30.html#ch30lev2sec4sec3).

## 31. STL Containers

1. An STL container defines a sequence; [§31.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev1sec2).

1. Use vector as your default container; [§31.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev1sec2), [§31.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev1sec4).

1. Insertion operators, such as insert() and push_back() are often more efficient on a vector than on a list; [§31.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev1sec2), [§31.4.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev3sec4sec1-1).

1. Use forward_list for sequences that are usually empty; [§31.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev1sec2), [§31.4.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev2sec4sec2).

1. When it comes to performance, don’t trust your intuition: measure; [§31.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev1sec3).

1. Don’t blindly trust asymptotic complexity measures; some sequences are short and the cost of individual operations can vary dramatically; [§31.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev1sec3).

1. STL containers are resource handles; [§31.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev2sec2sec1).

1. A map is usually implemented as a red-black tree; [§31.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev2sec2sec1), [§31.4.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev2sec4sec3).

1. An unordered_map is a hash table; [§31.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev2sec2sec1), [§31.4.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev3sec4sec3-2).

1. To be an element type for a STL container, a type must provide copy or move operations; [§31.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev2sec2sec2).

1. Use containers of pointers or smart pointers when you need to preserve polymorphic behavior; [§31.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev2sec2sec2).

1. Comparison operations should implement a strict weak order; [§31.2.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev3sec2sec2-1).

1. Pass a container by reference and return a container by value; [§31.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev2sec3sec2).

1. For a container, use the ()-initializer syntax for sizes and the {}-initializer syntax for lists of elements; [§31.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev2sec3sec2).

1. For simple traversals of a container, use a range-for loop or a begin/end pair of iterators; [§31.3.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev2sec3sec4).

1. Use const iterators where you don’t need to modify the elements of a container; [§31.3.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev2sec3sec4).

1. Use auto to avoid verbosity and typos when you use iterators; [§31.3.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev2sec3sec4).

1. Use reserve() to avoid invalidating pointers and iterators to elements; [§31.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev2sec3sec3), [§31.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev2sec4sec1).

1. Don’t assume performance benefits from reserve() without measurement; [§31.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev2sec3sec3).

1. Use push_back() or resize() on a container rather than realloc() on an array; [§31.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev2sec3sec3), [§31.4.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev3sec4sec1-1).

1. Don’t use iterators into a resized vector or deque; [§31.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev2sec3sec3).

1. When necessary, use reserve() to make performance predictable; [§31.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev2sec3sec3).

1. Do not assume that [] range checks; [§31.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev2sec2sec2).

1. Use at() when you need guaranteed range checks; [§31.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev2sec2sec2).

1. Use emplace() for notational convenience; [§31.3.7](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev2sec3sec7).

1. Prefer compact and contiguous data structures; [§31.4.1.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev3sec4sec1-2).

1. Use emplace() to avoid having to pre-initialize elements; [§31.4.1.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev3sec4sec1-3).

1. A list is relatively expensive to traverse; [§31.4.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev2sec4sec2).

1. A list usually has a four-word-per-element memory overhead; [§31.4.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev2sec4sec2).

1. The sequence of an ordered container is defined by its comparison object (by default \<); [§31.4.3.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev3sec4sec3-1).

1. The sequence of an unordered container (a hashed container) is not predictably ordered; [§31.4.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev3sec4sec3-2).

1. Use unordered containers if you need fast lookup for large amounts of data; [§31.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev1sec3).

1. Use unordered containers for element types with no natural order (e.g., no reasonable \<); [§31.4.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev2sec4sec3).

1. Use ordered associative containers (e.g., map and set) if you need to iterate over their elements in order; [§31.4.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev3sec4sec3-2).

1. Experiment to check that you have an acceptable hash function; [§31.4.3.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev3sec4sec3-4).

1. Hash functions obtained by combining standard hash functions for elements using exclusive or are often good; [§31.4.3.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev3sec4sec3-4).

1. 0.7 is often a reasonable load factor; [§31.4.3.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev3sec4sec3-5).

1. You can provide alternative interfaces for containers; [§31.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev1sec5).

1. The STL adaptors do not offer direct access to their underlying containers; [§31.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch31.html#ch31lev1sec5).

## 32. STL Algorithms

1. An STL algorithm operates on one or more sequences; [§32.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch32.html#ch32lev1sec2).

1. An input sequence is half-open and defined by a pair of iterators; [§32.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch32.html#ch32lev1sec2).

1. When searching, an algorithm usually returns the end of the input sequence to indicate “not found”; [§32.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch32.html#ch32lev1sec2).

1. Prefer a carefully specified algorithm to “random code”; [§32.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch32.html#ch32lev1sec2).

1. When writing a loop, consider whether it could be expressed as a general algorithm; [§32.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch32.html#ch32lev1sec2).

1. Make sure that a pair of iterator arguments really do specify a sequence; [§32.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch32.html#ch32lev1sec2).

1. When the pair-of-iterators style becomes tedious, introduce a container/range algorithm; [§32.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch32.html#ch32lev1sec2).

1. Use predicates and other function objects to give standard algorithms a wider range of meanings; [§32.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch32.html#ch32lev1sec3).

1. A predicate must not modify its argument; [§32.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch32.html#ch32lev1sec3).

1. The default == and \< on pointers are rarely adequate for standard algorithms; [§32.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch32.html#ch32lev1sec3).

1. Know the complexity of the algorithms you use, but remember that a complexity measure is only a rough guide to performance; [§32.3.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch32.html#ch32lev2sec3sec1).

1. Use for_each() and transform() only when there is no more-specific algorithm for a task; [§32.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch32.html#ch32lev2sec4sec1).

1. Algorithms do not directly add or subtract elements from their argument sequences; [§32.5.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch32.html#ch32lev2sec5sec2), [§32.5.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch32.html#ch32lev2sec5sec3).

1. If you have to deal with uninitialized objects, consider the uninitialized_* algorithms; [§32.5.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch32.html#ch32lev2sec5sec6).

1. An STL algorithm uses an equality comparison generated from its ordering comparison, rather than ==; [§32.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch32.html#ch32lev1sec6).

1. Note that sorting and searching C-style strings requires the user to supply a string comparison operation; [§32.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch32.html#ch32lev1sec6).

## 33. STL Iterators

1. An input sequence is defined by a pair of iterators; [§33.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch33.html#ch33lev2sec1sec1).

1. An output sequence is defined by a single iterator; avoid overflow; [§33.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch33.html#ch33lev2sec1sec1).

1. For any iterator p, [p:p] is the empty sequence; [§33.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch33.html#ch33lev2sec1sec1).

1. Use the end of a sequence to indicate “not found”; [§33.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch33.html#ch33lev2sec1sec1).

1. Think of iterators as more general and often better behaved pointers; [§33.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch33.html#ch33lev2sec1sec1).

1. Use iterator types, such as list\<char>::iterator, rather than pointers to refer to elements of a container; [§33.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch33.html#ch33lev2sec1sec1).

1. Use iterator_traits to obtain information about iterators; [§33.1.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch33.html#ch33lev2sec1sec3).

1. You can do compile-time dispatch using iterator_traits; [§33.1.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch33.html#ch33lev2sec1sec3).

1. Use iterator_traits to select an optimal algorithm based on an iterator’s category; [§33.1.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch33.html#ch33lev2sec1sec3).

1. iterator_traits are an implementation detail; prefer to use them implicitly; [§33.1.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch33.html#ch33lev2sec1sec3).

1. Use base() to extract an iterator from a reverse_iterator; [§33.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch33.html#ch33lev2sec2sec1).

1. You can use an insert iterator to add elements to a container; [§33.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch33.html#ch33lev2sec2sec2).

1. A move_iterator can be used to make copy operations into move operations; [§33.2.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch33.html#ch33lev2sec2sec3).

1. Make sure that your containers can be traversed using a range-for; [§33.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch33.html#ch33lev1sec3).

1. Use bind() to create variants of functions and function objects; [§33.5.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch33.html#ch33lev2sec5sec1).

1. Note that bind() dereferences references early; use ref() if you want to delay dereferencing; [§33.5.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch33.html#ch33lev2sec5sec1).

1. A mem_fn() or a lambda can be used to convert the p–>f(a) calling convention into f(p,a); [§33.5.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch33.html#ch33lev2sec5sec2).

1. Use function when you need a variable that can hold a variety of callable objects; [§33.5.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch33.html#ch33lev2sec5sec3).

## 34. Memory and Resources

1. Use array where you need a sequence with a constexpr size; [§34.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch34.html#ch34lev2sec2sec1).

1. Prefer array over built-in arrays; [§34.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch34.html#ch34lev2sec2sec1).

1. Use bitset if you need N bits and N is not necessarily the number of bits in a built-in integer type; [§34.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch34.html#ch34lev2sec2sec2).

1. Avoid vector\<bool>; [§34.2.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch34.html#ch34lev2sec2sec3).

1. When using pair, consider make_pair() for type deduction; [§34.2.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch34.html#ch34lev3sec2sec4-1).

1. When using tuple, consider make_tuple() for type deduction; [§34.2.4.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch34.html#ch34lev3sec2sec4-2).

1. Use unique_ptr to represent exclusive ownership; [§34.3.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch34.html#ch34lev2sec3sec1).

1. Use shared_ptr to represent shared ownership; [§34.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch34.html#ch34lev2sec3sec2).

1. Minimize the use of weak_ptrs; [§34.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch34.html#ch34lev2sec3sec3).

1. Use allocators (only) when the usual new/delete semantics is insufficient for logical or performance reasons; [§34.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch34.html#ch34lev1sec4).

1. Prefer resource handles with specific semantics to smart pointers; [§34.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch34.html#ch34lev1sec5).

1. Prefer unique_ptr to shared_ptr; [§34.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch34.html#ch34lev1sec5).

1. Prefer smart pointers to garbage collection; [§34.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch34.html#ch34lev1sec5).

1. Have a coherent and complete strategy for management of general resources; [§34.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch34.html#ch34lev1sec5).

1. Garbage collection can be really useful for dealing with leaks in programs with messy pointer use; [§34.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch34.html#ch34lev1sec5).

1. Garbage collection is optional; [§34.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch34.html#ch34lev1sec5).

1. Don’t disguise pointers (even if you don’t use garbage collection); [§34.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch34.html#ch34lev1sec5).

1. If you use garbage collection, use declare_no_pointers() to let the garbage collector ignore data that cannot contain pointers; [§34.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch34.html#ch34lev1sec5).

1. Don’t mess with uninitialized memory unless you absolutely have to; [§34.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch34.html#ch34lev1sec6).

## 35. Utilities

1. Use \<chrono> facilities, such as steady_clock, duration, and time_point for timing; [§35.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch35.html#ch35lev1sec2).

1. Prefer \<clock> facilities over \<ctime> facilities; [§35.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch35.html#ch35lev1sec2).

1. Use duration_cast to get durations in known units of time; [§35.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch35.html#ch35lev2sec2sec1).

1. Use system_clock::now() to get the current time; [§35.2.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch35.html#ch35lev2sec2sec3).

1. You can inquire about properties of types at compile time; [§35.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch35.html#ch35lev2sec4sec1).

1. Use move(obj) only when the value of obj cannot be used again; [§35.5.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch35.html#ch35lev2sec5sec1).

1. Use forward() for forwarding; [§35.5.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch35.html#ch35lev2sec5sec1).

## 36. Strings

1. Use character classifications rather than handcrafted checks on character ranges; [§36.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch36.html#ch36lev2sec2sec1).

1. If you implement string-like abstractions, use character_traits to implement operations on characters; [§36.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch36.html#ch36lev2sec2sec2).

1. A basic_string can be used to make strings of characters on any type; [§36.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch36.html#ch36lev1sec3).

1. Use strings as variables and members rather than as base classes; [§36.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch36.html#ch36lev1sec3).

1. Prefer string operations to C-style string functions; [§36.3.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch36.html#ch36lev2sec3sec1).

1. Return strings by value (rely on move semantics); [§36.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch36.html#ch36lev2sec3sec2).

1. Use string::npos to indicate “the rest of the string”; [§36.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch36.html#ch36lev2sec3sec2).

1. Do not pass a nullptr to a string function expecting a C-style string; [§36.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch36.html#ch36lev2sec3sec2).

1. A string can grow and shrink, as needed; [§36.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch36.html#ch36lev2sec3sec3).

1. Use at() rather than iterators or [] when you want range checking; [§36.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch36.html#ch36lev2sec3sec3), [§36.3.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch36.html#ch36lev2sec3sec6).

1. Use iterators and [] rather than at() when you want to optimize speed; [§36.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch36.html#ch36lev2sec3sec3), [§36.3.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch36.html#ch36lev2sec3sec6).

1. If you use strings, catch length_error and out_of_range somewhere; [§36.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch36.html#ch36lev2sec3sec3).

1. Use c_str() to produce a C-style string representation of a string (only) when you have to; [§36.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch36.html#ch36lev2sec3sec3).

1. string input is type sensitive and doesn’t overflow; [§36.3.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch36.html#ch36lev2sec3sec4).

1. Prefer a string_stream or a generic value extraction function (such as to\<X>) over direct use of str* numeric conversion functions; [§36.3.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch36.html#ch36lev2sec3sec5).

1. Use the find() operations to locate values in a string (rather than writing an explicit loop); [§36.3.7](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch36.html#ch36lev2sec3sec7).

1. Directly or indirectly, use substr() to read substrings and replace() to write substrings; [§36.3.8](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch36.html#ch36lev2sec3sec8).

## 37. Regular Expressions

1. Use regex for most conventional uses of regular expressions; [§37.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch37.html#ch37lev1sec1).

1. The regular expression notation can be adjusted to match various standards; [§37.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch37.html#ch37lev2sec1sec1), [§37.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch37.html#ch37lev1sec2).

1. The default regular expression notation is that of ECMAScript; [§37.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch37.html#ch37lev2sec1sec1).

1. For portability, use the character class notation to avoid nonstandard abbreviations; [§37.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch37.html#ch37lev2sec1sec1).

1. Be restrained; regular expressions can easily become a write-only language; [§37.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch37.html#ch37lev2sec1sec1).

1. Prefer raw string literals for expressing all but the simplest patterns; [§37.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch37.html#ch37lev2sec1sec1).

1. Note that \i allows you to express a subpattern in terms of a previous subpattern; [§37.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch37.html#ch37lev2sec1sec1).

1. Use ? to make patterns “lazy”; [§37.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch37.html#ch37lev2sec1sec1), [§37.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch37.html#ch37lev2sec2sec1).

1. regex can use ECMAScript, POSIX, awk, grep, and egrep notation; [§37.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch37.html#ch37lev1sec2).

1. Keep a copy of the pattern string in case you need to output it; [§37.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch37.html#ch37lev1sec2).

1. Use regex_search() for looking at streams of characters and regex_match() to look for fixed layouts; [§37.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch37.html#ch37lev2sec3sec2), [§37.3.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch37.html#ch37lev2sec3sec1).

## 38. I/O Streams

1. Define \<\< and >> for user-defined types with values that have meaningful textual representations; [§4.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch04.html#ch4lev2sec3sec3) [§38.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev1sec1), [§38.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev2sec4sec1), [§38.4.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev2sec4sec2).

1. Use cout for normal output and cerr for errors; [§38.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev1sec1).

1. There are iostreams for ordinary characters and wide characters, and you can define an iostream for any kind of character; [§38.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev1sec1).

1. There are standard iostreams for standard I/O streams, files, and strings; [§38.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev1sec2).

1. Don’t try to copy a file stream; [§38.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev2sec2sec1).

1. Binary I/O is system specific; [§38.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev2sec2sec1).

1. Remember to check that a file stream is attached to a file before using it; [§38.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev2sec2sec1).

1. Prefer ifstreams and ofstreams over the generic fstream; [§38.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev2sec2sec1).

1. Use stringstreams for in-memory formatting; [§38.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev2sec2sec2).

1. Use exceptions to catch rare bad() I/O errors; [§38.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev1sec3).

1. Use the stream state fail to handle potentially recoverable I/O errors; [§38.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev1sec3).

1. You don’t need to modify istream or ostream to add new \<\< and >> operators; [§38.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev2sec4sec1).

1. When implementing an iostream primitive operation, use sentry; [§38.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev2sec4sec1).

1. Prefer formatted input over unformatted, low-level input; [§38.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev2sec4sec1).

1. Input into strings does not overflow; [§38.4.1.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev3sec4sec1-2).

1. Be careful with the termination criteria when using get(), getline(), and read(); [§38.4.1.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev3sec4sec1-1).

1. By default >> skips whitespace; [§38.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev2sec4sec1).

1. You can define a \<\< (or a >>) so that it behaves as a virtual function based on its second operand; [§38.4.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev3sec4sec2-1).

1. Prefer manipulators to state flags for controlling I/O; [§38.4.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev2sec4sec3).

1. Use sync_with_stdio(true) if you want to mix C-style and iostream I/O; [§38.4.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev2sec4sec4).

1. Use sync_with_stdio(false) to optimize iostreams; [§38.4.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev2sec4sec4).

1. Tie streams used for interactive I/O; [§38.4.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev2sec4sec4).

1. Use imbue() to make an iostream reflect “cultural differences” of a locale; [§38.4.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev2sec4sec4).

1. width() specifications apply to the immediately following I/O operation only; [§38.4.5.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev3sec4sec5-1).

1. precision() specifications apply to all following floating-point output operations; [§38.4.5.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev3sec4sec5-1).

1. Floating-point format specifications (e.g., scientific) apply to all following floating-point output operations; [§38.4.5.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev3sec4sec5-2).

1. #include \<iomanip> when using standard manipulators taking arguments; [§38.4.5.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev3sec4sec5-2).

1. You hardly ever need to flush(); [§38.4.5.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev3sec4sec5-2).

1. Don’t use endl except possibly for aesthetic reasons; [§38.4.5.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev3sec4sec5-2).

1. If iostream formatting gets too tedious, write your own manipulators; [§38.4.5.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev3sec4sec5-3).

1. You can achieve the effect (and efficiency) of a ternary operator by defining a simple function object; [§38.4.5.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch38.html#ch38lev3sec4sec5-3).

## 39. Locales

1. Expect that every nontrivial program or system that interacts directly with people will be used in several different countries; [§39.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch39.html#ch39lev1sec1).

1. Don’t assume that everyone uses the same character set as you do; [§39.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch39.html#ch39lev1sec1), [§39.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch39.html#ch39lev2sec4sec1).

1. Prefer using locales to writing ad hoc code for culture-sensitive I/O; [§39.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch39.html#ch39lev1sec1).

1. Use locales to meet external (non-C++) standards; [§39.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch39.html#ch39lev1sec1).

1. Think of a locale as a container of facets; [§39.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch39.html#ch39lev1sec2).

1. Avoid embedding locale name strings in program text; [§39.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch39.html#ch39lev2sec2sec1).

1. Keep changes of locale to a few places in a program; [§39.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch39.html#ch39lev2sec2sec1).

1. Minimize the use of global format information; [§39.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch39.html#ch39lev2sec2sec1).

1. Prefer locale-sensitive string comparisons and sorts; [§39.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch39.html#ch39lev2sec2sec2), [§39.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch39.html#ch39lev2sec4sec1).

1. Make facets immutable; [§39.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch39.html#ch39lev1sec3).

1. Let locale handle the lifetime of facets; [§39.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch39.html#ch39lev1sec3).

1. You can make your own facets; [§39.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch39.html#ch39lev2sec3sec2).

1. When writing locale-sensitive I/O functions, remember to handle exceptions from user-supplied (overriding) functions; [§39.4.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch39.html#ch39lev3sec4sec2-2).

1. Use numput if you need separators in numbers; [§39.4.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch39.html#ch39lev3sec4sec2-1).

1. Use a simple Money type to hold monetary values; [§39.4.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch39.html#ch39lev2sec4sec3).

1. Use simple user-defined types to hold values that require locale-sensitive I/O (rather than casting to and from values of built-in types); [§39.4.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch39.html#ch39lev2sec4sec3).

1. The time_put facet can be used for both \<chrono>- and \<ctime>-style time; [§39.4.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch39.html#ch39lev2sec4sec4).

1. Prefer the character classification functions in which the locale is explicit; [§39.4.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch39.html#ch39lev2sec4sec5), [§39.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch39.html#ch39lev1sec5).

## 40. Numerics

1. Numerical problems are often subtle. If you are not 100% certain about the mathematical aspects of a numerical problem, either take expert advice, experiment, or do both; [§29.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch29.html#ch29lev1sec1).

1. Use variants of numeric types that are appropriate for their use; [§40.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch40.html#ch40lev1sec2).

1. Use numeric_limits to check that the numeric types are adequate for their use; [§40.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch40.html#ch40lev1sec2).

1. Specialize numeric_limits for a user-defined numeric type; [§40.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch40.html#ch40lev1sec2).

1. Prefer numeric_limits over limit macros; [§40.2.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch40.html#ch40lev2sec2sec1).

1. Use std::complex for complex arithmetic; [§40.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch40.html#ch40lev1sec4).

1. Use {} initialization to protect against narrowing; [§40.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch40.html#ch40lev1sec4).

1. Use valarray for numeric computation when run-time efficiency is more important than flexibility with respect to operations and element types; [§40.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch40.html#ch40lev1sec5).

1. Express operations on part of an array in terms of slices rather than loops; [§40.5.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch40.html#ch40lev2sec5sec5).

1. Slices is a generally useful abstraction for access of compact data; [§40.5.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch40.html#ch40lev2sec5sec4), [§40.5.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch40.html#ch40lev2sec5sec6).

1. Consider accumulate(), inner_product(), partial_sum(), and adjacent_difference() before you write a loop to compute a value from a sequence; [§40.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch40.html#ch40lev1sec6).

1. Bind an engine to a distribution to get a random number generator; [§40.7](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch40.html#ch40lev1sec7).

1. Be careful that your random numbers are sufficiently random; [§40.7.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch40.html#ch40lev2sec7sec1).

1. If you need genuinely random numbers (not just a pseudo-random sequence), use random_device; [§40.7.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch40.html#ch40lev2sec7sec2).

1. Prefer a random number class for a particular distribution over direct use of rand(); [§40.7.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch40.html#ch40lev2sec7sec4).

## 41. Concurrency

1. Use concurrency to improve responsiveness or to improve throughput; [§41.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch41.html#ch41lev1sec1).

1. Work at the highest level of abstraction that you can afford; [§41.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch41.html#ch41lev1sec1).

1. Prefer packaged_task and futures over direct use of threads and mutexes; [§41.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch41.html#ch41lev1sec1).

1. Prefer mutexes and condition_variables over direct use of atomics except for simple counters; [§41.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch41.html#ch41lev1sec1).

1. Avoid explicitly shared data whenever you can; [§41.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch41.html#ch41lev1sec1).

1. Consider processes as an alternative to threads; [§41.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch41.html#ch41lev1sec1).

1. The standard-library concurrency facilities are type safe; [§41.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch41.html#ch41lev1sec1).

1. The memory model exists to save most programmers from having to think about the machine architecture level of computers; [§41.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch41.html#ch41lev1sec2).

1. The memory model makes memory appear roughly as naively expected; [§41.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch41.html#ch41lev1sec2).

1. Separate threads accessing separate bit-fields of a struct may interfere with each other; [§41.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch41.html#ch41lev1sec2).

1. Avoid data races; [§41.2.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch41.html#ch41lev2sec2sec4).

1. Atomics allow for lock-free programming; [§41.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch41.html#ch41lev1sec3).

1. Lock-free programming can be essential for avoiding deadlock and to ensure that every thread makes progress; [§41.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch41.html#ch41lev1sec3).

1. Leave lock-free programming to experts; [§41.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch41.html#ch41lev1sec3).

1. Leave relaxed memory models to experts; [§41.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch41.html#ch41lev1sec3).

1. A volatile tells the compiler that the value of an object can be changed by something that is not part of the program; [§41.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch41.html#ch41lev1sec4).

1. A C++ volatile is not a synchronization mechanism; [§41.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch41.html#ch41lev1sec4).

## 42. Threads and Tasks

1. A thread is a type-safe interface to a system thread; [§42.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch42.html#ch42lev1sec2).

1. Do not destroy a running thread; [§42.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch42.html#ch42lev2sec2sec2).

1. Use join() to wait for a thread to complete; [§42.2.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch42.html#ch42lev2sec2sec4).

1. Consider using a guarded_thread to provide RAII for threads; [§42.2.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch42.html#ch42lev2sec2sec4).

1. Do not detach() a thread unless you absolutely have to; [§42.2.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch42.html#ch42lev2sec2sec4).

1. Use lock_guard or unique_lock to manage mutexes; [§42.3.1.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch42.html#ch42lev3sec3sec1-4).

1. Use lock() to acquire multiple locks; [§42.3.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch42.html#ch42lev2sec3sec2).

1. Use condition_variables to manage communication among threads; [§42.3.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch42.html#ch42lev2sec3sec4).

1. Think in terms of tasks that can be executed concurrently, rather than directly in terms of threads; [§42.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch42.html#ch42lev1sec4).

1. Value simplicity; [§42.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch42.html#ch42lev1sec4).

1. Return a result using a promise and get a result from a future; [§42.4.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch42.html#ch42lev2sec4sec1).

1. Don’t set_value() or set_exception() to a promise twice; [§42.4.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch42.html#ch42lev2sec4sec2).

1. Use packaged_tasks to handle exceptions thrown by tasks and to arrange for value return; [§42.4.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch42.html#ch42lev2sec4sec3).

1. Use a packaged_task and a future to express a request to an external service and wait for its response; [§42.4.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch42.html#ch42lev2sec4sec3).

1. Don’t get() twice from a future; [§42.4.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch42.html#ch42lev2sec4sec4).

1. Use async() to launch simple tasks; [§42.4.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch42.html#ch42lev2sec4sec6).

1. Picking a good granularity of concurrent tasks is difficult: experiment and measure; [§42.4.7](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch42.html#ch42lev2sec4sec7).

1. Whenever possible, hide concurrency behind the interface of a parallel algorithm; [§42.4.7](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch42.html#ch42lev2sec4sec7).

1. A parallel algorithm may be semantically different from a sequential solution to the same problem (e.g., pfind_all() vs. find()); [§42.4.7](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch42.html#ch42lev2sec4sec7).

1. Sometimes, a sequential solution is simpler and faster than a concurrent solution; [§42.4.7](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch42.html#ch42lev2sec4sec7).

## 43. The C Standard Library

1. Use fstreams rather than fopen()/fclose() if you worry about resource leaks; [§43.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch43.html#ch43lev1sec2).

1. Prefer \<iostream> to \<stdlib> for reasons of type safety and extensibility; [§43.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch43.html#ch43lev1sec3).

1. Never use gets() or scanf("%s",s); [§43.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch43.html#ch43lev1sec3).

1. Prefer \<string> to \<cstring> for reasons of ease of use and simplicity of resource management; [§43.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch43.html#ch43lev1sec4).

1. Use the C memory management routines, such as memcpy(), only for raw memory; [§43.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch43.html#ch43lev1sec5).

1. Prefer vector to uses of malloc() and realloc(); [§43.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch43.html#ch43lev1sec5).

1. Beware that the C standard library does not know about constructors and destructors; [§43.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch43.html#ch43lev1sec5).

1. Prefer \<chrono> to \<ctime> for timing; [§43.6](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch43.html#ch43lev1sec6).

1. For flexibility, ease of use, and performance, prefer sort() over qsort(); [§43.7](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch43.html#ch43lev1sec7).

1. Don’t use exit(); instead, throw an exception; [§43.7](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch43.html#ch43lev1sec7).

1. Don’t use longjmp(); instead, throw an exception; [§43.7](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch43.html#ch43lev1sec7).

## 44. Compatibility

1. Before using a new feature in production code, try it out by writing small programs to test the standards conformance and performance of the implementations you plan to use; [§44.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch44.html#ch44lev1sec1).

1. For learning C++, use the most up-to-date and complete implementation of Standard C++ that you can get access to; [§44.2.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch44.html#ch44lev2sec2sec4).

1. The common subset of C and C++ is not the best initial subset of C++ to learn; [§1.2.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch01.html#ch1lev2sec2sec3), [§44.2.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch44.html#ch44lev2sec2sec4).

1. Prefer standard facilities to nonstandard ones; [§36.1](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch36.html#ch36lev1sec1), [§44.2.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch44.html#ch44lev2sec2sec4).

1. Avoid deprecated features such as throw-specifications; [§44.2.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch44.html#ch44lev2sec2sec3), [§13.5.1.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch13.html#ch13lev3sec5sec1-3).

1. Avoid C-style casts; [§44.2.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch44.html#ch44lev2sec2sec3), [§11.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch11.html#ch11lev1sec5).

1. “Implicit int” has been banned, so explicitly specify the type of every function, variable, const, etc.; [§44.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch44.html#ch44lev2sec3sec3).

1. When converting a C program to C++, first make sure that function declarations (prototypes) and standard headers are used consistently; [§44.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch44.html#ch44lev2sec3sec3).

1. When converting a C program to C++, rename variables that are C++ keywords; [§44.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch44.html#ch44lev2sec3sec3).

1. For portability and type safety, if you must use C, write in the common subset of C and C++; [§44.2.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch44.html#ch44lev2sec2sec4).

1. When converting a C program to C++, cast the result of malloc() to the proper type or change all uses of malloc() to uses of new; [§44.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch44.html#ch44lev2sec3sec3).

1. When converting from malloc() and free() to new and delete, consider using vector, push_back(), and reserve() instead of realloc(); [§3.4.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch03.html#ch3lev2sec4sec2), [§43.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch43.html#ch43lev1sec5).

1. When converting a C program to C++, remember that there are no implicit conversions from ints to enumerations; use explicit type conversion where necessary; [§44.3.3](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch44.html#ch44lev2sec3sec3), [§8.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch08.html#ch8lev1sec4).

1. A facility defined in namespace std is defined in a header without a suffix (e.g., std::cout is declared in \<iostream>); [§30.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch30.html#ch30lev1sec2).

1. Use \<string> to get std::string (\<string.h> holds the C-style string functions); [§15.2.4](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch15.html#ch15lev2sec2sec4).

1. For each standard C header \<X.h> that places names in the global namespace, the header \<cX> places the names in namespace std; [§15.2.2](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch15.html#ch15lev2sec2sec2).

1. Use extern "C" when declaring C functions; [§15.2.5](https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/ch15.html#ch15lev2sec2sec5).
